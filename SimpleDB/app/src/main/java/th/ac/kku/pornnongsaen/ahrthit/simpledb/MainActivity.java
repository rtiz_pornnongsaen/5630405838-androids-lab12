package th.ac.kku.pornnongsaen.ahrthit.simpledb;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Record record;
    private DBHandler dbHandler;
    private TextView textView;
    private Button insertButton, viewButton, updateButton, deleteButton;
    private EditText idEditText, nameEditText;
    private List<Record> recordList;

    private final String INSERT = "insert", UPDATE = "update", DELETE = "delete", VIEWDB = "view";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //blind widget
        blindWidget();

        dbHandler = new DBHandler(getApplicationContext());

        insertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClick(INSERT);
            }
        });
        viewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClick(VIEWDB);
            }
        });
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClick(UPDATE);
            }
        });
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClick(DELETE);
            }
        });
    }

    private void buttonClick(String action) {
        if(!idEditText.getText().toString().trim().equals("")){
            int id = Integer.parseInt("" + idEditText.getText().toString().trim());
            String name = nameEditText.getText().toString().trim();
            record = new Record(id, name);
            switch (action){
                case UPDATE:
                    dbHandler.updateRecord(record);
                    Toast.makeText(getApplicationContext(), "update", Toast.LENGTH_LONG).show();
                    break;
                case DELETE:
                    recordList = new ArrayList<>();
                    recordList = dbHandler.getAllRecord();

                    for (int i = 0; i < recordList.size(); i++){
                        if(recordList.get(i).getId() == id){
                            dbHandler.deleteRecord(id);
                            textView.setText("Delete ID: " + id + " Success.");
                            break;
                        }else{
                            textView.setText("Not Found ID: " + id + ".");
                        }
                    }
                    Toast.makeText(getApplicationContext(), "delete", Toast.LENGTH_LONG).show();
                    break;
                case INSERT:
                    dbHandler.addRecord(record);
                    Toast.makeText(getApplicationContext(), "insert", Toast.LENGTH_LONG).show();
                    break;
                case VIEWDB:
                    Toast.makeText(getApplicationContext(), "view", Toast.LENGTH_LONG).show();

                    recordList = new ArrayList<>();
                    recordList = dbHandler.getAllRecord();

                    for (int i = 0; i < recordList.size(); i++){
                        if(recordList.get(i).getId() == id){
                            record = dbHandler.getRecord(id);
                            textView.setText(record.getId() + ". " + record.getName());
                            break;
                        }else{
                            textView.setText("Not Found ID: " + id + ".");
                        }
                    }
                    break;
            }
        }else{
            switch (action){
                case UPDATE:
                    Toast.makeText(getApplicationContext(), "Can not delete because ID is null.", Toast.LENGTH_LONG).show();
                    break;
                case DELETE:
                    Toast.makeText(getApplicationContext(), "Can not delete because ID is null.", Toast.LENGTH_LONG).show();
                    break;
                case INSERT:
                    Toast.makeText(getApplicationContext(), "You id is null must use AUTO Increment.", Toast.LENGTH_LONG).show();
                    break;
                case VIEWDB:
                    recordList = new ArrayList<>();
                    recordList = dbHandler.getAllRecord();

                    String text = "";
                    for (int i = 0; i < recordList.size(); i++){
                        text = text + recordList.get(i).getId() + ". " + recordList.get(i).getName() + "\n";
                    }
                    textView.setText(text);
                    Toast.makeText(getApplicationContext(), "Can not View because ID is null we will show all data.", Toast.LENGTH_LONG).show();
                    break;
            }
        }

    }

    private void blindWidget() {
        textView = (TextView) findViewById(R.id.textView);
        insertButton = (Button) findViewById(R.id.buttonInsert);
        viewButton = (Button) findViewById(R.id.buttonView);
        updateButton = (Button) findViewById(R.id.buttonUpdate);
        deleteButton = (Button) findViewById(R.id.buttonDelete);
        idEditText = (EditText) findViewById(R.id.editTextId);
        nameEditText = (EditText) findViewById(R.id.editTextName);
    }
}
