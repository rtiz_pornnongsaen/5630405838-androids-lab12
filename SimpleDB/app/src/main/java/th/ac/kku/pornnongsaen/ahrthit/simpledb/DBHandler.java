package th.ac.kku.pornnongsaen.ahrthit.simpledb;

/**
 * Created by Vorndervile on 9/5/2560.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vorndervile on 27/4/2560.
 */

public class DBHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static String DATABASE_NAME = "simpledb";
    private static String TABLE_RECORD = "record";
    private static String KEY_ID = "id";
    private static String KEY_NAME = "name";

    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_RECORD
                + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_NAME + " TEXT"
                + ");";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECORD);
        // Creating tables again
        onCreate(db);
    }

    // Adding new data
    public void addRecord(Record record) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, record.getId());
        values.put(KEY_NAME, record.getName());

        // Inserting Row
        db.insert(TABLE_RECORD, null, values);
        db.close(); // Closing database connection
        Log.d("DB_HELPER", "Add");
    }

    // Getting one shop
    public Record getRecord(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_RECORD, new String[]{KEY_ID,
                        KEY_NAME}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Record contact = new Record(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1));
        Log.d("DB_HELPER", "Get");
        // return shop
        return contact;
    }

    // Getting All data
    public List<Record> getAllRecord() {
        List<Record> recordList = new ArrayList<Record>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_RECORD;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Record record = new Record();
                record.setId(Integer.parseInt(cursor.getString(0)));
                record.setName(cursor.getString(1));
                // Adding contact to list
                recordList.add(record);
            } while (cursor.moveToNext());
        }

        Log.d("DB_HELPER", "Get All");

        // return contact list
        return recordList;
    }

    // Updating a shop
    public int updateRecord(Record record) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, record.getId());
        values.put(KEY_NAME, record.getName());

        Log.d("DB_HELPER", "Update");
        // updating row
        return db.update(TABLE_RECORD, values, KEY_ID + " = ?",
                new String[]{String.valueOf(record.getId())});

        //return 1 == update success
        //return 0 == update fail
    }

    // Deleting a shop
    public void deleteRecord(int id) {
        Log.d("DB_HELPER", "Delete");
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_RECORD, KEY_ID + " = ?",
                new String[] { String.valueOf(id) });
        db.close();
    }
}
