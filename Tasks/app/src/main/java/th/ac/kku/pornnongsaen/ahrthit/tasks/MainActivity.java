package th.ac.kku.pornnongsaen.ahrthit.tasks;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private TextView textView;
    private DBHandler dbHandler;
    private Record record;
    private List<Record> recordList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.textViewContent);

        dbHandler = new DBHandler(getApplicationContext());
        deleteAllRow();

        dbHandler.addRecord(new Record(1, "May 4 18:00-20:00 Quiz 2"));
        dbHandler.addRecord(new Record(2, "May 10 113:00-16:00 Final exam"));
        dbHandler.addRecord(new Record(3, "May 12 13:00-16:00 Final project presentation"));
        displayAllRows();

        textView.append("\nAdding new tasks\n");
        dbHandler.addRecord(new Record(4, "May 11 13:30-15:00 Talk about Property Tech"));
        displayById(4);
        dbHandler.addRecord(new Record(5, "May 19 Uploading Apps to Google Play"));
        displayById(5);

        textView.append("\nUpdating the existing task\n");
        dbHandler.updateRecord(new Record(3, "May 12 13:00-16:00 Final project presentation at room EN 4101"));
        displayById(3);

        textView.append("\nDisplaying all rows\n");
        displayAllRows();
    }

    private void deleteAllRow() {
        recordList = dbHandler.getAllRecord();
        for (int i = 0; i< recordList.size(); i++)
            dbHandler.deleteRecord(recordList.get(i).getId());
    }

    private void displayAllRows(){
        recordList = dbHandler.getAllRecord();
        for (int i = 0; i< recordList.size(); i++)
            textView.append(recordList.get(i).getId() + "." + recordList.get(i).getName() + "\n");
    }

    private void displayById(int id){
        record = dbHandler.getRecord(id);
        textView.append(record.getId() + "." + record.getName() + "\n");
    }
}
